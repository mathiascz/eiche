
//
// eiche/container/LinkedQueue.hpp
//
// Copyright (c) 2020-22, Mathias Czichi
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//    1. Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//    3. Neither the name of the copyright holder nor the names of its
//       contributors may be used to endorse or promote products derived
//       from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//

#ifndef eiche__container__LinkedQueue_hpp
#define eiche__container__LinkedQueue_hpp

#include "eiche/Type.hpp"
#include "eiche/container/LinkedNode.hpp"

#include <utility>
#include <type_traits>


namespace eiche {
namespace container {

struct LinkedQueue
   : eiche::Type<LinkedQueue>
{
   public:
      struct Traits
      {
         public:
            template <typename   T_Representation>
            struct GetNode
            {
               using type  =  typename T_Representation::Node;
            };
      };

   public:
      template <typename   T_Representation,
                typename   T_Structure>
      struct Assumption
         : std::true_type
      {
         static_assert(LinkedNode::IsRepresentation<typename Traits::template GetNode<T_Representation>::type>,
                       "is valid linked node representation");
      };

   public:
      template <typename   T_Queue>
      using Node  =  typename Traits::template GetNode<Representation<T_Queue>>::type;

   public:
      template <typename   T_Queue>
      static inline
      auto isEmpty(T_Queue&&  queue) noexcept
         -> bool
      {
         return Representation<T_Queue>::isEmpty(std::forward<T_Queue>(queue));
      }

      template <typename   T_Queue>
      static inline
      auto getFront(T_Queue&&    queue)
         -> decltype(Representation<T_Queue>::getFront(std::forward<T_Queue>(queue)))
      {
         return Representation<T_Queue>::getFront(std::forward<T_Queue>(queue));
      }

      template <typename   T_Queue>
      static inline
      auto getBack(T_Queue&&  queue)
         -> decltype(Representation<T_Queue>::getBack(std::forward<T_Queue>(queue)))
      {
         return Representation<T_Queue>::getBack(std::forward<T_Queue>(queue));
      }

      template <typename   T_Queue,
                typename   T_Node>
      static inline
      void pushBack(T_Queue&&    queue,
                    T_Node&&     node)
      {
         Representation<T_Queue>::pushBack(std::forward<T_Queue>(queue),
                                           std::forward<T_Node>(node));
      }

      template <typename   T_Queue>
      static inline
      void popFront(T_Queue&&    queue)
      {
         Representation<T_Queue>::popFront(std::forward<T_Queue>(queue));
      }
};

} // namespace container
} // namespace eiche
#endif // eiche__container__LinkedQueue_hpp
