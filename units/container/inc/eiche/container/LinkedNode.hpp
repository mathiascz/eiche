
//
// eiche/container/LinkedNode.hpp
//
// Copyright (c) 2020-22, Mathias Czichi
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//    1. Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//    3. Neither the name of the copyright holder nor the names of its
//       contributors may be used to endorse or promote products derived
//       from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//

#ifndef eiche__container__LinkedNode_hpp
#define eiche__container__LinkedNode_hpp

#include "eiche/Type.hpp"
#include "eiche/Reference.hpp"

#include <type_traits>
#include <utility>


namespace eiche {
namespace container {

struct LinkedNode
   : eiche::Type<LinkedNode>
{
   public:
      struct Traits
      {
         public:
            template <typename   T_Representation,
                      typename   T_Structure,
                      typename   = void>
            struct GetNode
            {
               using type  =  T_Structure;
            };

            template <typename   T_Representation,
                      typename   T_Structure>
            struct GetNode<T_Representation,
                           T_Structure,
                           std::void_t<typename T_Representation::Node>>
            {
               using type =   typename T_Representation::Node;
            };

         public:
            template <typename   T_Representation>
            struct GetReference
            {
               using type  =  typename T_Representation::Reference;
            };

         public:
            template <typename   T_Representation,
                      typename   T_Structure,
                      typename   = void>
            struct GetImplementation
            {
               using type  =  typename GetNode<T_Representation,
                                               T_Structure>::type;

               static constexpr
               bool const value  = false;
            };

            template <typename   T_Representation,
                      typename   T_Structure>
            struct GetImplementation<T_Representation,
                                     T_Structure,
                                     std::void_t<typename T_Representation::Implementation>>
            {
               using type  =  typename T_Representation::Implementation;

               static constexpr
               bool const value  =  not std::is_same_v<type,   T_Structure>;
            };
      };

   public:
      template <typename   T_Representation,
                typename   T_Structure,
                typename   T_Reference = typename Traits::template GetReference<T_Representation>::type,
                typename   T_GetImplementation = typename Traits::template GetImplementation<T_Representation,
                                                                                             T_Structure>,
                bool       T_HasImportedImplementation = T_GetImplementation::value>
      struct Assumption;

      template <typename   T_Representation,
                typename   T_Structure,
                typename   T_Reference,
                typename   T_GetImplementation>
      struct Assumption<T_Representation,
                        T_Structure,
                        T_Reference,
                        T_GetImplementation,
                        false>
         : std::true_type
      {
         static_assert(Reference::IsRepresentation<T_Reference>,
                       "valid reference representation");
      };

      template <typename   T_Representation,
                typename   T_Structure,
                typename   T_Reference,
                typename   T_GetImplementation>
      struct Assumption<T_Representation,
                        T_Structure,
                        T_Reference,
                        T_GetImplementation,
                        true>
         : std::true_type
      {
         static_assert(Reference::IsRepresentation<T_Reference>,
                       "valid reference representation");
         static_assert(LinkedNode::IsRepresentation<typename T_GetImplementation::type>,
                       "valid imported linked node implementation representation");
      };

   public:
      template <typename   T_Structure>
      using Node = typename Traits::template GetNode<Representation<T_Structure>,
                                                     T_Structure>::type;

      template <typename   T_Structure>
      using Reference = typename Traits::template GetReference<Representation<T_Structure>>::type;

      template <typename   T_Structure>
      using Implementation = typename Traits::template GetImplementation<Representation<T_Structure>,
                                                                         T_Structure>::type;

      template <typename   T_Structure>
      static inline
      constexpr bool HasImportedImplementation = Traits::template GetImplementation<Representation<T_Structure>,
                                                                                    T_Structure>::value;

   public:
      template <typename   T_Node>
      static inline
      auto getNil() noexcept
         -> decltype(Representation<T_Node>::getNil())
      {
         return Representation<T_Node>::getNil();
      }

      template <typename   T_Node>
      static inline
      auto getSuccessor(T_Node&&    node) noexcept
         -> decltype(Representation<T_Node>::getSuccessor(std::forward<T_Node>(node)))
      {
         return Representation<T_Node>::getSuccessor(std::forward<T_Node>(node));
      }

      template <typename   T_Node,
                typename   T_Reference>
      static inline
      void setSuccessor(T_Node&&       node,
                        T_Reference&&  ref) noexcept
      {
         return Representation<T_Node>::setSuccessor(std::forward<T_Node>(node),
                                                     std::forward<T_Reference>(ref));
      }

      template <typename   T_Node>
      static inline
      auto asImplementation(T_Node&&   node) noexcept
         -> decltype(Representation<T_Node>::asImplementation(std::forward<T_Node>(node)))
      {
         return Representation<T_Node>::asImplementation(std::forward<T_Node>(node));
      }

      template <typename   T_Node,
                typename   T_Implementation>
      static inline
      auto asNode(T_Implementation&&   impl) noexcept
         -> decltype(Representation<T_Node>::asNode(std::forward<T_Implementation>(impl)))
      {
         return Representation<T_Node>::asNode(std::forward<T_Implementation>(impl));
      }
};

} // namespace container
} // namespace eiche
#endif // eiche__container__LinkedNode_hpp
