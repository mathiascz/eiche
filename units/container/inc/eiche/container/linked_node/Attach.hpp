
//
// eiche/container/linked_node/Attach.hpp
//
// Copyright (c) 2020-22, Mathias Czichi
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//    1. Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//    3. Neither the name of the copyright holder nor the names of its
//       contributors may be used to endorse or promote products derived
//       from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//

#ifndef eiche__container__linked_node__Attach_hpp
#define eiche__container__linked_node__Attach_hpp

#include "eiche/container/LinkedNode.hpp"
#include "eiche/reference/WrappedPointer.hpp"
#include "eiche/ToOuterCast.hpp"

#include <cstdint>
#include <limits>
#include <utility>


namespace eiche {
namespace container {
namespace linked_node {

template <typename   T_Structure,
          typename   T_Implementation,
          typename   T_Tag = void>
struct Attach
{
   public:
      T_Implementation     m_Implementation;

   public:
      template <typename...   T_Arguments>
      Attach(T_Arguments&&...    args)
         : m_Implementation(std::forward<T_Arguments>(args)...)
      { }

   public:
      ~Attach() = default;

   public:
      struct traits {

         template <typename   T_Type, bool = true>
         struct Representation
         { };

         template <bool    B>
         struct Representation<eiche::container::LinkedNode,   B>
         {
            using type  =  Representation;

            public:
               using Node              =  T_Structure;
               using Reference         =  reference::WrappedPointer<Node>;
               using Implementation    =  T_Implementation;

            public:
               static inline
               auto getNil() noexcept
                  -> Reference
               {
                  return reinterpret_cast<Node*>(std::numeric_limits<std::uintptr_t>::max());
               }

               static
               auto getSuccessor(Node&    node)
                  -> Reference
               {
                  LinkedNode::Reference<Implementation> implementationRef = LinkedNode::getSuccessor(static_cast<Attach&>(node).m_Implementation);

                  if (not eiche::Reference::isAttached(implementationRef))
                  {
                     return eiche::Reference::getUnattached<Reference>();
                  }
                  else if (implementationRef == LinkedNode::getNil<Implementation>())
                  {
                     return getNil();
                  }
                  else
                  {
                     Implementation& implementation = eiche::Reference::resolve<Implementation>(implementationRef);

                     return eiche::Reference::make<Reference>(static_cast<Node&>(eiche::toOuterCast(implementation,
                                                                                                    &Attach::m_Implementation)));
                  }
               }

               static
               void setSuccessor(Node&             node,
                                 Reference const   successor)
               {
                  Implementation& implementation = static_cast<Attach&>(node).m_Implementation;

                  if (not eiche::Reference::isAttached(successor))
                  {
                     LinkedNode::setSuccessor(implementation,
                                              eiche::Reference::getUnattached<LinkedNode::Reference<Implementation>>());
                  }
                  else if (successor == getNil())
                  {
                     LinkedNode::setSuccessor(implementation,
                                              LinkedNode::getNil<Implementation>());
                  }
                  else
                  {
                     LinkedNode::setSuccessor(implementation,
                                              eiche::Reference::make<LinkedNode::Reference<Implementation>>(eiche::Reference::resolve<Node>(successor).m_Implementation));
                  }
               }

               static inline
               auto asImplementation(Node&   node) noexcept
                  -> Implementation&
               {
                  return static_cast<Attach&>(node).m_Implementation;
               }

               static inline
               auto asNode(Implementation&   implementation) noexcept
                  -> Node&
               {
                  return static_cast<Node&>(eiche::toOuterCast(implementation,
                                                               &Attach::m_Implementation));
               }
         };

      };
};

} // namespace linked_node
} // namespace container
} // namespace eiche
#endif // eiche__container__linked_node__Attach_hpp
