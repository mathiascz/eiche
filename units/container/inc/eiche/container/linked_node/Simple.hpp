
//
// eiche/container/linked_node/Simple.hpp
//
// Copyright (c) 2020-22, Mathias Czichi
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//    1. Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//    3. Neither the name of the copyright holder nor the names of its
//       contributors may be used to endorse or promote products derived
//       from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//

#ifndef eiche__container__linked_node__Simple_hpp
#define eiche__container__linked_node__Simple_hpp

#include "eiche/container/LinkedNode.hpp"
#include "eiche/reference/WrappedPointer.hpp"

#include <cstdint>
#include <limits>


namespace eiche {
namespace container {
namespace linked_node {

struct Simple
{
   public:
      using Reference   =  reference::WrappedPointer<Simple>;

   public:
      Reference   m_Successor;

   public:
      constexpr
      Simple() noexcept
         : m_Successor()
      { }

      constexpr explicit
      Simple(Reference const  successor) noexcept
         : m_Successor(successor)
      { }

   public:
      ~Simple() = default;

   public:
      Simple(Simple const&) = delete;
      Simple& operator=(Simple const&) = delete;

   public:
      struct traits {

          template <typename   T_Type, bool = true>
          struct Representation
          { };

          template <bool   B>
          struct Representation<eiche::container::LinkedNode,  B>
          {
             using type =  Representation;

             public:
                using Reference  =  Simple::Reference;

             public:
                static inline
                auto getNil() noexcept
                   -> Reference
                {
                   return reinterpret_cast<Simple*>(std::numeric_limits<std::uintptr_t>::max());
                }

             public:
                static inline
                auto getSuccessor(Simple const&   node) noexcept
                   -> Reference
                {
                   return node.m_Successor;
                }

                static inline
                void setSuccessor(Simple&         node,
                                  Reference const   successor) noexcept
                {
                   node.m_Successor = successor;
                }
          };

      };
};

} // namespace linked_node
} // namespace container
} // namespace eiche
#endif // eiche__container__linked_node__Simple_hpp
