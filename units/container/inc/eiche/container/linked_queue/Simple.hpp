
//
// eiche/container/linked_queue/Simple.hpp
//
// Copyright (c) 2020-22, Mathias Czichi
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//    1. Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//    3. Neither the name of the copyright holder nor the names of its
//       contributors may be used to endorse or promote products derived
//       from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//

#ifndef eiche__container__linked_queue__Simple_hpp
#define eiche__container__linked_queue__Simple_hpp

#include "eiche/container/LinkedQueue.hpp"
#include "eiche/Reference.hpp"

#include <utility>


namespace eiche {
namespace container {
namespace linked_queue {

namespace simple {
namespace detail {

template <typename   T_Structure,
          bool       T_HasImportedImplementation = eiche::container::LinkedNode::HasImportedImplementation<T_Structure>>
struct Facilities;

template <typename   T_Structure>
struct Facilities<T_Structure,
                  false>
{
   public:
      using Node  =  T_Structure;

   public:
      LinkedNode::Reference<Node>   m_Front;
      LinkedNode::Reference<Node>   m_Back;

   public:
      inline
      Facilities() noexcept
         : m_Front(LinkedNode::getNil<Node>())
         , m_Back(LinkedNode::getNil<Node>())
      { }

   public:
      ~Facilities() = default;

   public:
      Facilities(Facilities const&) = delete;
      Facilities& operator=(Facilities const&) = delete;

      inline
      Facilities(Facilities&&    src)
         : m_Front(std::move(src.m_Front))
         , m_Back(std::move(src.m_Back))
      {
         src.m_Front = LinkedNode::getNil<Node>();
         src.m_Back = LinkedNode::getNil<Node>();
      }

      Facilities& operator=(Facilities&&) = delete;

   public:
      static inline
      auto isEmpty(Facilities const&   queue) noexcept
         -> bool
      {
         return queue.m_Front == LinkedNode::getNil<Node>();
      }

      template <typename   T_Facilities>
      static inline
      auto getFront(T_Facilities const&   queue)
         -> decltype(eiche::Reference::resolve(queue.m_Front))
      {
         return eiche::Reference::resolve(queue.m_Front);
      }

      template <typename   T_Facilities>
      static inline
      auto getBack(T_Facilities const&    queue)
         -> decltype(eiche::Reference::resolve(queue.m_Back))
      {
         return eiche::Reference::resolve(queue.m_Back);
      }

      template <typename   T_Node>
      static
      void pushBack(Facilities&     queue,
                    T_Node&&        node)
      {
         bool const isFirstNode = isEmpty(queue);
         auto ref = eiche::Reference::make<LinkedNode::Reference<Node>>(std::forward<T_Node>(node));

         if (not isFirstNode)
         {
            LinkedNode::setSuccessor(eiche::Reference::resolve(queue.m_Back),
                                     ref);
         }
         else
         {
            queue.m_Front = ref;
         }

         LinkedNode::setSuccessor(node,
                                  LinkedNode::getNil<Node>());
         queue.m_Back = std::move(ref);
      }

      static
      void popFront(Facilities&  queue)
      {
         if (not isEmpty(queue))
         {
            decltype(eiche::Reference::resolve(queue.m_Front)) prevFront = eiche::Reference::resolve(queue.m_Front);

            queue.m_Front = LinkedNode::getSuccessor(prevFront);

            LinkedNode::setSuccessor(prevFront,
                                     eiche::Reference::getUnattached<LinkedNode::Reference<Node>>());

            if (queue.m_Front == LinkedNode::getNil<Node>())
            {
               queue.m_Back = LinkedNode::getNil<Node>();
            }
         }
      }

   public:
      struct traits {

         template <typename   T_Type,  bool = true>
         struct Representation
         { };

         template <bool    B>
         struct Representation<eiche::container::LinkedQueue,  B>
         {
            using type  =  Facilities;
         };

      };
};


template <typename   T_Structure>
struct Facilities<T_Structure,
                  true>
   : private Facilities<LinkedNode::Implementation<T_Structure>>
{
   private:
      using Heritage =  Facilities<LinkedNode::Implementation<T_Structure>>;

   public:
      using Node  =  T_Structure;

   public:
      using Heritage::m_Front;
      using Heritage::m_Back;

   public:
      Facilities() = default;

   public:
      ~Facilities() = default;

   public:
      Facilities(Facilities const&) = delete;
      Facilities& operator=(Facilities const&) = delete;

      Facilities(Facilities&&) = default;
      Facilities& operator=(Facilities&&) = delete;

   public:
      static inline
      auto isEmpty(Facilities const&   queue) noexcept
         -> bool
      {
         return Heritage::isEmpty(queue);
      }

      static inline
      auto getFront(Facilities const&  queue)
         -> decltype(LinkedNode::asNode<Node>(Heritage::getFront(std::declval<Heritage const&>())))
      {
         return LinkedNode::asNode<Node>(Heritage::getFront(queue));
      }

      static inline
      auto getBack(Facilities const&   queue)
         -> decltype(LinkedNode::asNode<Node>(Heritage::getBack(std::declval<Heritage const&>())))
      {
         return LinkedNode::asNode<Node>(Heritage::getBack(queue));
      }

      template <typename   T_Node>
      static inline
      void pushBack(Facilities&     queue,
                    T_Node&&        node)
      {
         Heritage::pushBack(queue,
                            LinkedNode::asImplementation(std::forward<T_Node>(node)));
      }

      static inline
      void popFront(Facilities&  queue)
      {
         Heritage::popFront(queue);
      }

   public:
      struct traits {

         template <typename   T_Type,  bool = true>
         struct Representation
         { };

         template <bool    B>
         struct Representation<eiche::container::LinkedQueue,  B>
         {
            using type  =  Facilities;
         };

      };
};

} // namespace detail
} // namespace simple


template <typename   T_Structure>
using Simple   =  simple::detail::Facilities<T_Structure>;

} // namespace linked_queue
} // namespace container
} // namespace eiche
#endif // eiche__container__linked_queue__Simple_hpp
