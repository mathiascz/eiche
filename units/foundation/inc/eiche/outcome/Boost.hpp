
//
// eiche/outcome/Boost.hpp
//
// Copyright (c) 2022, Mathias Czichi
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//    1. Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//    3. Neither the name of the copyright holder nor the names of its
//       contributors may be used to endorse or promote products derived
//       from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//

#ifndef eiche__outcome__Boost_hpp
#define eiche__outcome__Boost_hpp

#include "eiche/Outcome.hpp"

#include <boost/outcome.hpp>


namespace eiche {
namespace type {

template <typename   T_Value,
          typename   T_Error,
          typename   T_Exception,
          typename   T_NoValuePolicy>
struct Representation<eiche::Outcome,
                      BOOST_OUTCOME_V2_NAMESPACE::outcome<T_Value,
                                                          T_Error,
                                                          T_Exception,
                                                          T_NoValuePolicy>>
{
   using type  =  Representation;

   using Outcome = BOOST_OUTCOME_V2_NAMESPACE::outcome<T_Value,
                                                       T_Error,
                                                       T_Exception,
                                                       T_NoValuePolicy>;

   using Value =  T_Value;
   using Error =  T_Error;

   static inline
   auto hasValue(Outcome const&  o)
      -> bool
   {
      return o.has_value();
   }

   static inline
   auto hasError(Outcome const&  o)
      -> bool
   {
      return o.has_error();
   }

   template <typename   T_Outcome>
   static inline
   auto getValue(T_Outcome&&  o)
      -> decltype(o.value())
   {
      return o.value();
   }

   template <typename   T_Outcome>
   static inline
   auto getError(T_Outcome&&  o)
      -> decltype(o.error())
   {
      return o.error();
   }

   template <typename   ...T_Arguments>
   static inline
   auto success(T_Arguments&&...    args)
      -> decltype(BOOST_OUTCOME_V2_NAMESPACE::success(std::forward<T_Arguments>(args)...))
   {
      return BOOST_OUTCOME_V2_NAMESPACE::success(std::forward<T_Arguments>(args)...);
   }

   template <typename   ...T_Arguments>
   static inline
   auto failure(T_Arguments&&...    args)
      -> decltype(BOOST_OUTCOME_V2_NAMESPACE::failure(std::forward<T_Arguments>(args)...))
   {
      return BOOST_OUTCOME_V2_NAMESPACE::failure(std::forward<T_Arguments>(args)...);
   }
};

} // namespace type
} // namespace eiche
#endif // eiche__outcome__Boost_hpp
