
//
// eiche/reference/WrappedPointer.hpp
//
// Copyright (c) 2020-22, Mathias Czichi
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//    1. Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//    3. Neither the name of the copyright holder nor the names of its
//       contributors may be used to endorse or promote products derived
//       from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//

#ifndef eiche__reference__WrappedPointer_hpp
#define eiche__reference__WrappedPointer_hpp

#include "eiche/Reference.hpp"

#include <memory>
#include <type_traits>


namespace eiche {
namespace reference {

template <typename   T_Structure>
struct WrappedPointer
{
   public:
      T_Structure*   m_Pointer;

   public:
      inline constexpr
      WrappedPointer()
         : m_Pointer(nullptr)
      { }

      inline constexpr
      WrappedPointer(T_Structure* const   ptr)
         : m_Pointer(ptr)
      { }

   public:
      ~WrappedPointer() = default;

   public:
      WrappedPointer(WrappedPointer const&) = default;
      WrappedPointer& operator=(WrappedPointer const&) = default;

   public:
      inline
      bool operator==(WrappedPointer const&     other) const noexcept
      {
         return m_Pointer == other.m_Pointer;
      }

      inline
      bool operator!=(WrappedPointer const&     other) const noexcept
      {
         return m_Pointer != other.m_Pointer;
      }

   public:
      struct traits {

         template <typename   T_Type,  bool = true>
         struct Representation
         { };

         template <bool    B>
         struct Representation<eiche::Reference,   B>
         {
            using type  =  Representation;

            public:
               using Element  =  T_Structure;

            public:
               static inline constexpr
               auto getUnattached() noexcept
                  -> WrappedPointer
               {
                  return WrappedPointer();
               }

               static inline
               auto isAttached(WrappedPointer const&  ref) noexcept
                  -> bool
               {
                  return ref.m_Pointer != nullptr;
               }

               static inline
               auto make(T_Structure&  element) noexcept
                  -> WrappedPointer
               {
                  return std::addressof(element);
               }

               static inline
               auto resolve(WrappedPointer const&  ref) noexcept
                  -> T_Structure&
               {
                  return *ref.m_Pointer;
               }
         };

      };
};


template <>
struct WrappedPointer<void>
{
   public:
      void*    m_Pointer;

   public:
      inline constexpr
      WrappedPointer()
         : m_Pointer(nullptr)
      { }

      inline constexpr
      WrappedPointer(void* const    ptr)
         : m_Pointer(ptr)
      { }

   public:
      ~WrappedPointer() = default;

   public:
      WrappedPointer(WrappedPointer const&) = default;
      WrappedPointer& operator=(WrappedPointer const&) = default;

   public:
      inline
      bool operator==(WrappedPointer const&     other) const noexcept
      {
         return m_Pointer == other.m_Pointer;
      }

      inline
      bool operator!=(WrappedPointer const&     other) const noexcept
      {
         return m_Pointer != other.m_Pointer;
      }

   public:
      struct traits {

         template <typename   T_Type,  bool = true>
         struct Representation
         { };

         template <bool    B>
         struct Representation<eiche::Reference,   B>
         {
            using type  =  Representation;

            public:
               template <typename   T_Structure>
               using IsElement = std::true_type;

            public:
               static inline constexpr
               auto getUnattached() noexcept
                  -> WrappedPointer
               {
                  return WrappedPointer();
               }

               static inline
               auto isAttached(WrappedPointer const&  ref) noexcept
                  -> bool
               {
                  return ref.m_Pointer != nullptr;
               }

               template <typename   T_Element>
               static inline
               auto isAttached(WrappedPointer const&  ref) noexcept
                  -> bool
               {
                  return isAttached(ref);
               }

               template <typename   T_Structure>
               static inline
               auto make(T_Structure&  element) noexcept
                  -> WrappedPointer
               {
                  return std::addressof(element);
               }

               template <typename   T_Element>
               static inline
               auto resolve(WrappedPointer const&  ref) noexcept
                  -> T_Element&
               {
                  return *reinterpret_cast<T_Element*>(ref.m_Pointer);
               }
         };

      };
};

} // namespace reference
} // namespace eiche
#endif // eiche__reference__WrappedPointer_hpp
