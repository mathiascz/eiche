
//
// eiche/Outcome.hpp
//
// Copyright (c) 2022, Mathias Czichi
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//    1. Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//    3. Neither the name of the copyright holder nor the names of its
//       contributors may be used to endorse or promote products derived
//       from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//

#ifndef eiche__Outcome_hpp
#define eiche__Outcome_hpp

#include "eiche/Type.hpp"

#include <tuple>
#include <utility>


namespace eiche {

struct Outcome;


namespace outcome {
namespace detail {

template <typename   T_IndexSequence,
          typename   ...T_Arguments>
struct Success;

template <std::size_t   ...T_Indices,
          typename      ...T_Arguments>
struct Success<std::index_sequence<T_Indices...>,
               T_Arguments...>
{
   using Tuple = std::tuple<T_Arguments...>;

   Tuple    m_Arguments;

   template <typename   ...T_Args>
   inline
   Success(T_Args&&...  args)
      : m_Arguments(std::forward<T_Args>(args)...)
   { }

   template <typename   T_Outcome>
   inline
   operator T_Outcome()
   {
      return eiche::type::GetRepresentation_t<eiche::Outcome,
                                              T_Outcome>::success(std::forward<T_Arguments>(std::get<T_Indices>(m_Arguments))...);
   }
};

} // namespace detail

template <typename   T_Value>
struct Success
{
   T_Value  m_Value;

   Success(T_Value   value)
      : m_Value(static_cast<T_Value>(value))
   { }

   template <typename   T_Outcome>
   inline
   operator T_Outcome()
   {
      return eiche::type::GetRepresentation_t<eiche::Outcome,
                                              T_Outcome>::success(static_cast<T_Value>(m_Value));
   }
};

template <>
struct Success<void>
{
   Success() = default;

   template <typename   T_Outcome>
   inline
   operator T_Outcome()
   {
      return eiche::type::GetRepresentation_t<eiche::Outcome,
                                              T_Outcome>::success();
   }
};


template <typename   T_Value>
struct Failure
{
   T_Value  m_Value;

   Failure(T_Value   value)
      : m_Value(static_cast<T_Value>(value))
   { }

   template <typename   T_Outcome>
   inline
   operator T_Outcome()
   {
      return eiche::type::GetRepresentation_t<eiche::Outcome,
                                              T_Outcome>::failure(static_cast<T_Value>(m_Value));
   }
};

template <>
struct Failure<void>
{
   Failure() = default;

   template <typename   T_Outcome>
   inline
   operator T_Outcome()
   {
      return eiche::type::GetRepresentation_t<eiche::Outcome,
                                              T_Outcome>::failure();
   }
};

} // namespace outcome

struct Outcome
   : eiche::Type<Outcome>
{
   //
   // properties
   //
   template <typename   T_Outcome>
   using Value = typename Representation<T_Outcome>::Value;

   template <typename   T_Outcome>
   using Error = typename Representation<T_Outcome>::Error;

   //
   // operations
   //
   template <typename   T_Outcome>
   static inline
   auto hasValue(T_Outcome const&   o)
      -> bool
   {
      return Representation<T_Outcome>::hasValue(o);
   }

   template <typename   T_Outcome>
   static inline
   auto hasError(T_Outcome const&   o)
      -> bool
   {
      return Representation<T_Outcome>::hasError(o);
   }

   template <typename   T_Outcome>
   static inline
   auto getValue(T_Outcome&&  o)
      -> decltype(Representation<T_Outcome>::getValue(std::forward<T_Outcome>(o)))
   {
      return Representation<T_Outcome>::getValue(std::forward<T_Outcome>(o));
   }

   template <typename   T_Outcome>
   static inline
   auto getError(T_Outcome&&  o)
      -> decltype(Representation<T_Outcome>::getError(std::forward<T_Outcome>(o)))
   {
      return Representation<T_Outcome>::getError(std::forward<T_Outcome>(o));
   }

   //
   // generic constructors
   //
   template <typename   T_Value>
   static inline
   auto success()
      -> outcome::Success<void>
   {
      return outcome::Success<void>();
   }

   template <typename   T_Value>
   static inline
   auto success(T_Value&&  value)
      -> outcome::Success<decltype(value)>
   {
      return outcome::Success<decltype(value)>(std::forward<T_Value>(value));
   }

   template <typename   T_Value>
   static inline
   auto failure()
      -> outcome::Failure<void>
   {
      return outcome::Failure<void>();
   }

   template <typename   T_Value>
   static inline
   auto failure(T_Value&&  value)
      -> outcome::Failure<decltype(value)>
   {
      return outcome::Failure<decltype(value)>(std::forward<T_Value>(value));
   }
};

} // namespace eiche
#endif // eiche__Outcome_hpp
