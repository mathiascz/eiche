
//
// eiche/tuple/Stl.hpp
//
// Copyright (c) 2021, Mathias Czichi
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//    1. Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//    3. Neither the name of the copyright holder nor the names of its
//       contributors may be used to endorse or promote products derived
//       from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//

#ifndef eiche__tuple__Stl_hpp
#define eiche__tuple__Stl_hpp

#include "eiche/Tuple.hpp"

#include <tuple>


namespace eiche {
namespace type {

template <typename   ...T_Elements>
struct Representation<eiche::Tuple,
                      std::tuple<T_Elements...>>
{
   using type = Representation;

   //
   // properties
   //
   using Elements = std::tuple<T_Elements...>;

   //
   // operations
   //
   template <std::size_t   T_Index,
             typename      T_Tuple>
   static inline constexpr
   auto get(T_Tuple&&   t) noexcept
      -> decltype(std::get<T_Index>(std::forward<T_Tuple>(t)))
   {
      return std::get<T_Index>(std::forward<T_Tuple>(t));
   }
};

} // namespace type
} // namespace eiche
#endif // eiche__tuple__Stl_hpp
