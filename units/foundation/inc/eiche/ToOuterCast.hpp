
//
// eiche/ToOuterCast.hpp
//
// Copyright (c) 2020, Mathias Czichi
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//    1. Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//    3. Neither the name of the copyright holder nor the names of its
//       contributors may be used to endorse or promote products derived
//       from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//

#ifndef eiche__ToOuterCast_hpp
#define eiche__ToOuterCast_hpp

#include "eiche/traits/AssignQualification.hpp"
   //    eiche::traits::AssignQualification_t<>

#include <type_traits>
   //    std::remove_cv<>


namespace eiche {

template <typename   T_Structure,
          typename   T_OuterStructure>
inline
auto toOuterCast(T_Structure&    from,
                 T_Structure     (T_OuterStructure::*member)) noexcept
   -> T_OuterStructure&
{
   typedef eiche::traits::AssignQualification_t<char,
                                                T_Structure>
           Char;

   typedef eiche::traits::AssignQualification_t<T_OuterStructure,
                                                T_Structure>
           Outer;

   return *reinterpret_cast<Outer*>(reinterpret_cast<Char*>(&from) -
                                    (reinterpret_cast<Char*>(&((*static_cast<Outer*>(nullptr)).*member)) - static_cast<Char*>(nullptr)));
}

} // namespace eiche
#endif // eiche__ToOuterCast_hpp
