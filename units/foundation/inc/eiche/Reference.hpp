
//
// eiche/Reference.hpp
//
// Copyright (c) 2020-22, Mathias Czichi
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//    1. Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//    3. Neither the name of the copyright holder nor the names of its
//       contributors may be used to endorse or promote products derived
//       from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//

#ifndef eiche__Reference_hpp
#define eiche__Reference_hpp

#include "eiche/Type.hpp"
#include "eiche/traits/Identity.hpp"

#include <utility>
#include <type_traits>


namespace eiche {

struct Reference
   : eiche::Type<Reference>
{
   public:
      struct Traits
      {
         public:
            template <typename   T_Representation,
                      typename   =  void>
            struct HasElementMember
               : std::false_type
            { };                                                                        

            template <typename   T_Representation>
            struct HasElementMember<T_Representation,
                                    std::void_t<typename T_Representation::Element>>
               : std::true_type
            { };

            template <typename   T_Representation,
                      typename   = void>
            struct HasIsElementMember
               : std::false_type
            { };

            template <typename   T_Representation>
            struct HasIsElementMember<T_Representation,
                                      std::void_t<typename T_Representation::template IsElement<void>>>
               : std::true_type
            { };

            template <typename   T_Representation>
            using IsVariant = HasIsElementMember<T_Representation>;

         public:
            template <typename   T_Representation>
            struct GetElement
            {
               using type  =  typename T_Representation::Element;
            };

         public:
            template <typename   T_Representation,
                      typename   T_Structure,
                      bool       T_IsVariant = IsVariant<T_Representation>::value>
            struct IsElement;

            template <typename   T_Representation,
                      typename   T_Structure>
            struct IsElement<T_Representation,
                             T_Structure,
                             false>
               : std::is_same<T_Structure,
                              typename GetElement<T_Representation>::type>
            { };

            template <typename   T_Representation,
                      typename   T_Structure>
            struct IsElement<T_Representation,
                             T_Structure,
                             true>
               : std::integral_constant<bool,
                                        T_Representation::template IsElement<T_Structure>::value>
            { };

         public:
            template <typename   T_Representation,
                      bool       T_IsVariant = IsVariant<T_Representation>::value>
            struct Resolve;

            template <typename   T_Representation>
            struct Resolve<T_Representation,
                           false>
            {
               template <typename   T_Element,
                         typename   T_Reference>
               static inline
               auto apply(T_Reference&&   r)
                  -> decltype(T_Representation::resolve(std::forward<T_Reference>(r)))
               {
                  static_assert(    std::is_same_v<T_Element,  void>
                                 or std::is_same_v<T_Element,  typename GetElement<T_Representation>::type>,
                                "valid reference element");

                  return T_Representation::resolve(std::forward<T_Reference>(r));
               }
            };

            template <typename   T_Representation>
            struct Resolve<T_Representation,
                           true>
            {
               template <typename   T_Element,
                         typename   T_Reference>
               static inline
               auto apply(T_Reference&&   r)
                  -> decltype(T_Representation::template resolve<T_Element>(std::forward<T_Reference>(r)))
               {
                  static_assert(       not std::is_same_v<T_Element,    void>
                                 and   IsElement<T_Representation, T_Element>::value,
                                "valid reference element");

                  return T_Representation::template resolve<T_Element>(std::forward<T_Reference>(r));
               }
            };

      };

   public:
      template <typename   T_Representation,
                typename   T_Structure,
                bool       T_IsVariant = Traits::template IsVariant<T_Representation>::value>
      struct Assumption;

      template <typename   T_Representation,
                typename   T_Structure>
      struct Assumption<T_Representation,
                        T_Structure,
                        false>
         : std::true_type
      {
         static_assert(Traits::template HasElementMember<T_Representation>::value,
                       "non variant reference has unique element");
         static_assert((not Traits::template HasIsElementMember<T_Representation>::value),
                       "non variant reference has not variant element query");
      };

      template <typename   T_Representation,
                typename   T_Structure>
      struct Assumption<T_Representation,
                        T_Structure,
                        true>
         : std::true_type
      {
         static_assert((not Traits::template HasElementMember<T_Representation>::value),
                       "variant reference has not unique element");
         static_assert(Traits::template HasIsElementMember<T_Representation>::value,
                       "variant reference has variant element query");
      };

   public:
      template <typename   T_Reference>
      static inline
      constexpr bool IsVariant = Traits::template IsVariant<Representation<T_Reference>>::value;

      template <typename   T_Reference>
      using Element = typename Traits::template GetElement<Representation<T_Reference>>::type;

      template <typename   T_Reference,
                typename   T_Structure>
      static inline
      constexpr bool IsElement = Traits::template IsElement<Representation<T_Reference>,
                                                            T_Structure>::value;

   public:
      template <typename   T_Reference>
      static inline
      auto getUnattached() noexcept
         -> decltype(Representation<T_Reference>::getUnattached())
      {
         return Representation<T_Reference>::getUnattached();
      }

      template <typename   T_Reference>
      static inline
      auto isAttached(T_Reference const&  r) noexcept
         -> bool
      {
         return Representation<T_Reference>::isAttached(r);
      }

      template <typename   T_Element,
                typename   T_Reference>
      static inline
      auto isAttached(T_Reference const&  r) noexcept
         -> bool
      {
         static_assert(IsElement<T_Reference,
                                 T_Element>,
                       "is valid structure");

         if constexpr (IsVariant<T_Reference>)
         {
            return Representation<T_Reference>::template isAttached<T_Element>(r);
         }
         else
         {
            return Representation<T_Reference>::isAttached(r);
         }
      }

      template <typename   T_Reference,
                typename   T_Element>
      static inline
      auto make(T_Element&&   element)
         -> decltype(Representation<T_Reference>::make(std::forward<T_Element>(element)))
      {
         static_assert(IsElement<T_Reference,
                                 std::remove_reference_t<T_Element>>,
                       "is valid element");

         return Representation<T_Reference>::make(std::forward<T_Element>(element));
      }

      template <typename   T_Element = void,
                typename   T_Reference>
      static inline
      auto resolve(T_Reference&&    r)
         -> decltype(Traits::Resolve<Representation<T_Reference>>::template apply<T_Element>(std::forward<T_Reference>(r)))
      {
         return Traits::Resolve<Representation<T_Reference>>::template apply<T_Element>(std::forward<T_Reference>(r));
      }
};

} // namespace eiche
#endif // eiche__Reference_hpp
