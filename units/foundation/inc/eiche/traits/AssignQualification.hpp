
//
// eiche/traits/AssignQualification.hpp
//
// Copyright (c) 2020, Mathias Czichi
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//    1. Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//    3. Neither the name of the copyright holder nor the names of its
//       contributors may be used to endorse or promote products derived
//       from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//

#ifndef eiche__traits__AssignQualification_hpp
#define eiche__traits__AssignQualification_hpp

#include <type_traits>
   //    std::remove_cv<>
   //    std::remove_const<>
   //    std::remove_volatile<>
   //    std::conditional<>
   //    std::is_const<>
   //    std::is_volatile<>
   //    std::add_const<>
   //    std::add_volatile<>


namespace eiche {
namespace traits {

template <typename   T_To,
          typename   T_From>
using AssignConstQualification = typename std::conditional<std::is_const<T_From>::value,
                                                           typename std::add_const<T_To>::type,
                                                           typename std::remove_const<T_To>::type>;

template <typename   T_To,
          typename   T_From>
using AssignConstQualification_t = typename AssignConstQualification<T_To,
                                                                     T_From>::type;


template <typename   T_To,
          typename   T_From>
using AssignVolatileQualification = typename std::conditional<std::is_volatile<T_From>::value,
                                                              typename std::add_volatile<T_To>::type,
                                                              typename std::remove_volatile<T_To>::type>;

template <typename   T_To,
          typename   T_From>
using AssignVolatileQualification_t = typename AssignVolatileQualification<T_To,
                                                                           T_From>::type;


template <typename   T_To,
          typename   T_From>
using AssignQualification = AssignVolatileQualification<AssignConstQualification_t<T_To,
                                                                                   T_From>,
                                                        T_From>;

template <typename   T_To,
          typename   T_From>
using AssignQualification_t = typename AssignQualification<T_To,
                                                           T_From>::type;


} // namespace traits
} // namespace eiche
#endif // eiche__traits__AssignQualification_hpp
