
//
// eiche/pac/Find.hpp
//
// Copyright (c) 2020, Mathias Czichi
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//    1. Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//    3. Neither the name of the copyright holder nor the names of its
//       contributors may be used to endorse or promote products derived
//       from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//

#ifndef eiche__pac__Find_hpp
#define eiche__pac__Find_hpp

#include "eiche/pac/Fold.hpp"
   //    eiche::pac::FoldWhile<>

#include "eiche/pac/PushBack.hpp"
   //    eiche::pac::PushBack_t<>

#include <type_traits>
   //    std::conditional_t<>
   //    std::is_same<>


namespace eiche {
namespace pac {
namespace detail {
namespace find_if {

struct NotFound;

template <typename   T_Front,
          typename   T_Element = NotFound,
          bool       T_NotFound = true>
struct State;


template <typename   T_Front>
struct State<T_Front,
             NotFound,
             true>
{
   typedef T_Front      Front;
};

template <typename   T_Front,
          typename   T_Element>
struct State<T_Front,
             T_Element,
             true>
{
   typedef pac::PushBack_t<T_Front,
                           T_Element>
           Front;
};

template <typename   T_Front,
          typename   T_Element>
struct State<T_Front,
             T_Element,
             false>
{
   typedef T_Front      Front;

   typedef T_Element    type;
};


template <typename   T_Predicate>
struct Function
{
   template <typename   T_State,
             typename   T_Element>
   struct apply
   {
      static
      constexpr bool const value = not T_Predicate::template apply<T_Element>::value;

      typedef State<typename T_State::Front,
                    T_Element,
                    value>
              type;
   };
};


template <typename   T_Pac,
          typename   T_Predicate>
class Apply;


template <typename   T_Result,
          typename   T_Remainder>
struct Result
   : T_Result
{
   typedef T_Remainder     Tail;
};



template <template <typename...> class    T_Pac,
          typename...                     T_Elements,
          typename                        T_Predicate>
class Apply<T_Pac<T_Elements...>,
            T_Predicate>
{
   private:
      typedef pac::FoldWhile<T_Pac<T_Elements...>,
                             State<T_Pac<>>,
                             Function<T_Predicate>>
              FoldResult;

   public:
      typedef Result<typename FoldResult::type,
                     typename FoldResult::Remainder>
              type;
};

} // namespace find_if
} // namespace detail

template <typename   T_Pac,
          typename   T_Predicate>
using FindIf = typename detail::find_if::Apply<T_Pac,
                                               T_Predicate>::type;

template <typename   T_Pac,
          typename   T_Predicate>
using FindIf_t = typename FindIf<T_Pac,
                                 T_Predicate>::type;

namespace detail {
namespace find {

template <typename   T_Structure>
struct Function
{
   template <typename   T_Element>
   using apply = std::is_same<T_Element,
                              T_Structure>;
};

} // namespace find
} // namespace detail

template <typename   T_Pac,
          typename   T_Structure>
using Find = FindIf<T_Pac,
                    detail::find::Function<T_Structure>>;

template <typename   T_Pac,
          typename   T_Structure>
using Find_t = typename Find<T_Pac,
                             T_Structure>::type;

} // namespace pac
} // namespace eiche
#endif // eiche__pac__Find_hpp
