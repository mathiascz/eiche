
//
// eiche/pac/PushFront.hpp
//
// Copyright (c) 2020, Mathias Czichi
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//    1. Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//    3. Neither the name of the copyright holder nor the names of its
//       contributors may be used to endorse or promote products derived
//       from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//

#ifndef eiche__pac__PushFront_hpp
#define eiche__pac__PushFront_hpp

namespace eiche {
namespace pac {
namespace detail {

template <typename   T_Pac,
          typename   T_Element>
struct PushFront;

template <template <typename...> class    T_Pac,
          typename...                     T_Elements,
          typename                        T_Element>
struct PushFront<T_Pac<T_Elements...>,
                 T_Element>
{
   typedef T_Pac<T_Element,
                 T_Elements...>
           type;
};

} // namespace detail

template <typename   T_Pac,
          typename   T_Element>
using PushFront = detail::PushFront<T_Pac,
                                    T_Element>;

template <typename   T_Pac,
          typename   T_Element>
using PushFront_t = typename PushFront<T_Pac,
                                       T_Element>::type;

} // namespace pac
} // namespace eiche
#endif // eiche__pac__PushFront_hpp
