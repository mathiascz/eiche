
//
// eiche/pac/Fold.hpp
//
// Copyright (c) 2020, Mathias Czichi
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//    1. Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//    3. Neither the name of the copyright holder nor the names of its
//       contributors may be used to endorse or promote products derived
//       from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//

#ifndef eiche__pac__Fold_hpp
#define eiche__pac__Fold_hpp

namespace eiche {
namespace pac {
namespace detail {
namespace fold_while {

template <typename   T_Pac,
          typename   T_State,
          typename   T_Function>
struct Apply;

template <template <typename...> class    T_Pac,
          typename                        T_State,
          typename                        T_Function>
struct Apply<T_Pac<>,
             T_State,
             T_Function>
{
   typedef T_State      type;
   typedef T_Pac<>      Remainder;
};


template <typename   T_Pac,
          typename   T_Result,
          typename   T_Function,
          bool       T_Condition = T_Result::value>
struct Evaluate;

template <template <typename...> class    T_Pac,
          typename                        T_Head,
          typename...                     T_Tail,
          typename                        T_Result,
          typename                     T_Function>
struct Evaluate<T_Pac<T_Head,
                      T_Tail...>,
                T_Result,
                T_Function,
                true>
   : Apply<T_Pac<T_Tail...>,
           typename T_Result::type,
           T_Function>
{
};

template <template <typename...> class    T_Pac,
          typename                        T_Head,
          typename...                     T_Tail,
          typename                        T_Result,
          typename                        T_Function>
struct Evaluate<T_Pac<T_Head,
                      T_Tail...>,
                T_Result,
                T_Function,
                false>
{
   typedef typename T_Result::type     type;
   typedef T_Pac<T_Tail...>            Remainder;
};


template <template <typename...> class    T_Pac,
          typename                        T_Head,
          typename...                     T_Tail,
          typename                        T_State,
          typename                        T_Function>
struct Apply<T_Pac<T_Head,
                   T_Tail...>,
             T_State,
             T_Function>
   : Evaluate<T_Pac<T_Head,
                    T_Tail...>,
              typename T_Function::template apply<T_State,
                                                  T_Head>,
              T_Function>
{
};

} // namespace fold_while
} // namespace detail

template <typename   T_Pac,
          typename   T_State,
          typename   T_Function>
using FoldWhile = detail::fold_while::Apply<T_Pac,
                                            T_State,
                                            T_Function>;

template <typename   T_Pac,
          typename   T_State,
          typename   T_Function>
using FoldWhile_t = typename FoldWhile<T_Pac,
                                       T_State,
                                       T_Function>::type;


namespace detail {
namespace fold {

template <typename   T_Function>
struct Function
{
   template <typename   T_State,
             typename   T_Element>
   struct apply
   {
      static
      constexpr bool const value = true;

      typedef typename T_Function::template apply<T_State,
                                                  T_Element>::type
              type;
   };
};

} // namespace fold
} // namespace detail

template <typename   T_Pac,
          typename   T_State,
          typename   T_Function>
using Fold = FoldWhile<T_Pac,
                       T_State,
                       detail::fold::Function<T_Function>>;

template <typename   T_Pac,
          typename   T_State,
          typename   T_Function>
using Fold_t = typename Fold<T_Pac,
                             T_State,
                             T_Function>::type;

} // namespace pac
} // namespace eiche
#endif // eiche__pac__Fold_hpp
