
//
// eiche/pac/At.hpp
//
// Copyright (c) 2020, Mathias Czichi
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//    1. Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//    3. Neither the name of the copyright holder nor the names of its
//       contributors may be used to endorse or promote products derived
//       from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//

#ifndef eiche__pac__At_hpp
#define eiche__pac__At_hpp

#include <cstddef>
   //    std::size_t


namespace eiche {
namespace pac {

namespace detail {

template <std::size_t   T_Index,
          typename...   T_Elements>
struct At;

template <typename      T_Head,
          typename...   T_Tail>
struct At<0u,
          T_Head,
          T_Tail...>
{
   typedef T_Head    type;
};

template <std::size_t   T_Index,
          typename      T_Head,
          typename...   T_Tail>
struct At<T_Index,
          T_Head,
          T_Tail...>
   : At<T_Index - 1u,
        T_Tail...>
{
};

} // namespace detail

template <typename      T_Pac,
          std::size_t   T_Index>
struct At;

template <template <typename...> class    T_Pac,
          typename...                     T_Elements,
          std::size_t                     T_Index>
struct At<T_Pac<T_Elements...>,
          T_Index>
   : detail::At<T_Index,
                T_Elements...>
{
};

template <typename      T_Pac,
          std::size_t   T_Index>
using At_t = typename At<T_Pac,
                         T_Index>::type;

} // namespace pac
} // namespace eiche
#endif // eiche__pac__At_hpp
