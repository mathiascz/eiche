
//
// eiche/pac/Utilize.hpp
//
// Copyright (c) 2020, Mathias Czichi
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//    1. Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//    3. Neither the name of the copyright holder nor the names of its
//       contributors may be used to endorse or promote products derived
//       from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//

#ifndef eiche__pac__Utilize_hpp
#define eiche__pac__Utilize_hpp

#include <utility>
   //    std::forward<>()


namespace eiche {
namespace pac {

namespace detail {

namespace utilize {

template <typename...   T_Elements>
struct Utilize
{
   template <typename      T_Function,
             typename...   T_Arguments>
   static
   void apply(T_Function&        /*f*/,
              T_Arguments&&...   /*args*/)
   {
   }
};

template <typename      T_Head,
          typename...   T_Tail>
struct Utilize<T_Head,
               T_Tail...>
{
   template <typename      T_Function,
             typename...   T_Arguments>
   static
   void apply(T_Function&        f,
              T_Arguments&&...   args)
   {
      f.template operator()<T_Head>(std::forward<T_Arguments>(args)...);
      Utilize<T_Tail...>::apply(f,
                                std::forward<T_Arguments>(args)...);
   }
};

} // namespace utilize

template <typename   T_Pac>
struct Utilize;

template <template <typename...> class    T_Pac,
          typename...                     T_Elements>
struct Utilize<T_Pac<T_Elements...>>
   : utilize::Utilize<T_Elements...>
{
};

} // namespace detail

template <typename      T_Pac,
          typename      T_Function,
          typename...   T_Arguments>
T_Function utilize(T_Function&&        f,
                   T_Arguments&&...    args)
{
   detail::Utilize<T_Pac>::apply(f,
                                 std::forward<T_Arguments>(args)...);

   return std::forward<T_Function>(f);
}

} // namespace pac
} // namespace eiche
#endif // eiche__pac__Utilize_hpp
