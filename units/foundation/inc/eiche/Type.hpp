
//
// eiche/Type.hpp
//
// Copyright (c) 2020-21, Mathias Czichi
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//    1. Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//    2. Redistributions in binary form must reproduce the above
//       copyright notice, this list of conditions and the following
//       disclaimer in the documentation and/or other materials provided
//       with the distribution.
//    3. Neither the name of the copyright holder nor the names of its
//       contributors may be used to endorse or promote products derived
//       from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
// FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
// COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
// BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
// CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
// LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
// WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//

#ifndef eiche__Type_hpp
#define eiche__Type_hpp

#include <type_traits>
#include <utility>


namespace eiche {
namespace type {

template <typename   T_Type>
struct Specify;


template <typename   T_Symbol>
struct IsSpecification
   : std::is_base_of<Specify<T_Symbol>,
                     T_Symbol>
{ };

template <typename   T_Symbol>
inline
constexpr bool IsSpecification_v = IsSpecification<T_Symbol>::value;


template <typename   T_Type,
          typename   T_Symbol>
struct Representation;

template <typename   T_Type,
          typename   T_Symbol,
          typename   T_Predicate = void>
struct Mapping
{ };


//
// GetRepresentation<>
//
namespace detail {

template <typename   T_Symbol>
using Strip_t = std::remove_cv_t<std::remove_reference_t<T_Symbol>>;



template <typename   T_Representation,
          typename   = void>
struct IsComplete
   : std::false_type
{ };

template <typename   T_Representation>
struct IsComplete<T_Representation,
                  std::void_t<typename T_Representation::type>>
   : std::true_type
{ };

template <typename   T_Representation>
inline
constexpr bool IsComplete_v = IsComplete<T_Representation>::value;



template <typename   T_Representation,
          typename   T_Symbol,
          typename   = void>
struct IsDeferred
   : std::false_type
{ };

template <typename   T_Representation,
          typename   T_Symbol>
struct IsDeferred<T_Representation,
                  T_Symbol,
                  std::void_t<typename T_Representation::template Bind<T_Symbol>::type>>
   : std::true_type
{ };

template <typename   T_Representation,
          typename   T_Symbol>
inline
constexpr bool IsDeferred_v = IsDeferred<T_Representation,
                                         T_Symbol>::value;



template <typename   T_Type,
          typename   T_Symbol,
          typename   T_Representation = type::Representation<T_Type,
                                                             Strip_t<T_Symbol>>,
          bool       T_IsComplete = IsComplete_v<T_Representation>,
          bool       T_IsDeferred = IsDeferred_v<T_Representation,
                                                 T_Symbol>>
struct BindRepresentation
{ };

template <typename   T_Type,
          typename   T_Symbol,
          typename   T_Representation>
struct BindRepresentation<T_Type,
                          T_Symbol,
                          T_Representation,
                          true,              // T_IsComplete
                          false>             // T_IsDeferred
{
   using type  =  typename T_Representation::type;
};

template <typename   T_Type,
          typename   T_Symbol,
          typename   T_Representation>
struct BindRepresentation<T_Type,
                          T_Symbol,
                          T_Representation,
                          false,             // T_IsComplete
                          true>              // T_IsDeferred
{
   using type  =  typename T_Representation::template Bind<T_Symbol>::type;
};

template <typename   T_Type,
          typename   T_Symbol>
using BindRepresentation_t = typename BindRepresentation<T_Type,
                                                         T_Symbol>::type;



template <typename   T_Type,
          typename   T_Symbol,
          typename   = void>
struct IsRepresentation
   : std::false_type
{ };

template <typename   T_Type,
          typename   T_Symbol>
struct IsRepresentation<T_Type,
                        T_Symbol,
                        std::void_t<BindRepresentation_t<T_Type,
                                                         T_Symbol>>>
   : std::true_type
{ };

} // namespace detail


template <typename   T_Type,
          typename   T_Symbol>
using IsRepresentation = detail::IsRepresentation<T_Type,
                                                  T_Symbol>;

template <typename   T_Type,
          typename   T_Symbol>
inline
constexpr bool IsRepresentation_v = IsRepresentation<T_Type,
                                                     T_Symbol>::value;


template <typename   T_Type,
          typename   T_Symbol>
struct GetRepresentation
   : detail::BindRepresentation<T_Type,
                                T_Symbol>
{
   static_assert(IsSpecification_v<T_Type>,
                 "is type specification");

   static_assert(IsRepresentation_v<T_Type,
                                    T_Symbol>,
                 "is representation for the type");
};

template <typename   T_Type,
          typename   T_Symbol>
using GetRepresentation_t = typename GetRepresentation<T_Type,
                                                       T_Symbol>::type;


//
// Representation<>
//
namespace detail {

template <typename   T_Type,
          typename   T_Symbol,
          typename   = void>
struct HasRepresentationTraits
   : std::false_type
{ };

template <typename   T_Type,
          typename   T_Symbol>
struct HasRepresentationTraits<T_Type,
                               T_Symbol,
                               std::void_t<typename T_Symbol::traits::template Representation<T_Type>>>
   : std::true_type
{ };

template <typename   T_Type,
          typename   T_Symbol>
inline
constexpr bool HasRepresentationTraits_v = HasRepresentationTraits<T_Type,
                                                                   T_Symbol>::value;



template <typename   T_Type,
          typename   T_Symbol,
          bool       T_HasRepresentationTraits = HasRepresentationTraits_v<T_Type,
                                                                           T_Symbol>>
struct DefaultRepresentation
   : BindRepresentation<T_Type,
                        T_Symbol,
                        typename T_Symbol::traits::template Representation<T_Type>>
{ };

template <typename   T_Type,
          typename   T_Symbol>
struct DefaultRepresentation<T_Type,
                             T_Symbol,
                             false>        // T_HasRepresentationTraits
   : Mapping<T_Type,
             T_Symbol>
{ };

} // namespace detail

template <typename   T_Type,
          typename   T_Symbol>
struct Representation
   : detail::DefaultRepresentation<T_Type,
                                   T_Symbol>
{ };


//
// Specify<>
//
template <typename   T_Type>
struct Specify
{
   using Current  =  T_Type;

   template <typename   T_Symbol>
   static inline
   constexpr bool IsRepresentation = type::IsRepresentation_v<T_Type,
                                                              T_Symbol>;

   template <typename   T_Symbol>
   using Representation = type::GetRepresentation_t<T_Type,
                                                    T_Symbol>;
};

} // namespace type

template <typename   T_Type>
using Type = type::Specify<T_Type>;

template <typename   T_Symbol>
using IsType = type::IsSpecification<T_Symbol>;

} // namespace eiche
#endif // eiche__Type_hpp
