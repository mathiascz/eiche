EICHE is the acronym for
   (E)xtensible
   (I)ntegratable
   (C)omponent
   (H)omogenous
   (E)xecutive.
It is a meta-framework with the primary goal to provide a common
C++ application component development approach based on Carl Hewitts
"Actor-Model" independently from the actual runtime environment, that
may range from very small resource constrained bare metal operated
µ-Controllers up to large distributed enterprise server clusters.
